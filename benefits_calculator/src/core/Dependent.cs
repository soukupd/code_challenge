using System;
using benefits_calculator.core.Models;
using benefits_calculator.core.Interfaces;

namespace benefits_calculator.core.Interfaces
{
    public interface IDependent
    {
        Guid ID { get; set; }
        decimal BenefitCost { get; }
    }
}

namespace benefits_calculator.core.Models
{
    public class Dependent : Person, IDependent
    {
        public Guid ID { get; set; }
        public decimal BenefitCost { get; set; }
        private IEmployee _dependee { get; set; }
        public Dependent(IEmployee dependee)
        {
            _dependee = dependee;
        }
    }
}