using System;
using System.Collections.Generic;
using benefits_calculator.core.Models;
using benefits_calculator.core.Interfaces;

namespace benefits_calculator.core.Interfaces
{
    public interface IEmployer
    {
        Guid ID { get; set; }
        int LoginID { get; set; }
        string Name { get; set; }
        IEnumerable<IEmployee> Employees { get; }
    }
}

namespace benefits_calculator.core.Models
{

    public class Employer : IEmployer
    {
        public Guid ID { get; set; }
        public int LoginID { get; set; }
        public string Name { get; set; }
        public IEnumerable<IEmployee> Employees { get; } = new List<IEmployee>();
    }
}