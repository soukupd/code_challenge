using System;
using System.Collections.Generic;
using benefits_calculator.core.Models;
using benefits_calculator.core.Interfaces;

namespace benefits_calculator.core.Interfaces
{
    public interface IEmployee
    {
        Guid ID { get; set; }
        decimal AnnualPay { get; set; }
        int PayPeriods { get; set; }
        decimal BenefitCost { get; set; }
        IEnumerable<IDependent> Dependents { get; }
    }
}

namespace benefits_calculator.core.Models
{

    public class Employee : Person, IEmployee
    {
        public Guid ID { get; set; }
        public decimal AnnualPay { get; set; }
        public int PayPeriods { get; set; }
        public decimal BenefitCost { get; set; }
        public IEnumerable<IDependent> Dependents { get; } = new List<IDependent>();
        private Guid _employerID { get; set; }
        public Employee(Guid employerID)
        {
            _employerID = employerID;
        }
    }
}