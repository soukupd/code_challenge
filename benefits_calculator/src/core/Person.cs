using System.Collections.Generic;
using benefits_calculator.core.Interfaces;
using benefits_calculator.core.Models;

namespace benefits_calculator.core.Interfaces
{
    public interface IPerson
    {
        string FirstName { get; set; }
        string LastName { get; set; }
    }
}

namespace benefits_calculator.core.Models
{
    public class Person : IPerson
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}







