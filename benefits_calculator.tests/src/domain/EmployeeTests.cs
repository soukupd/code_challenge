using NUnit.Framework;
using Moq;
using System;
using benefits_calculator.core.Models;
using benefits_calculator.core.Interfaces;

namespace benefits_calculator.core.UnitTests
{
    [TestFixture]
    public class EmployeeClass
    {
        private Employee _employee;

        [SetUp]
        public void Init()
        {
            _employee = new Employee(new Guid());
        }

        [Test]
        public void ShouldImplementEmployeeInterface()
        {
            Assert.IsInstanceOf<IEmployee>(_employee);
        }
    }
}