using NUnit.Framework;
using Moq;
using benefits_calculator.core.Models;
using benefits_calculator.core.Interfaces;

namespace benefits_calculator.UnitTests
{
    [TestFixture]
    public class PersonClass
    {
        private Person _person;

        [SetUp]
        public void Init()
        {
            _person = new Person();
        }

        [Test]
        public void ShouldImplementPersonInterface()
        {
            Assert.IsInstanceOf<IPerson>(_person);
        }
    }
}