using NUnit.Framework;
using Moq;
using benefits_calculator.core.Models;
using benefits_calculator.core.Interfaces;

namespace benefits_calculator.core.UnitTests
{
    [TestFixture]
    public class EmployerClass
    {
        private Employer _employer;

        [SetUp]
        public void Init()
        {
            _employer = new Employer();
        }

        [Test]
        public void ShouldImplementEmployeeInterface()
        {
            Assert.IsInstanceOf<IEmployer>(_employer);
        }
    }
}