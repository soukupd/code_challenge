using NUnit.Framework;
using Moq;
using benefits_calculator.core.Models;
using benefits_calculator.core.Interfaces;

namespace benefits_calculator.core.UnitTests
{
    [TestFixture]
    public class DependentClass
    {
        private Dependent _dependent;

        [SetUp]
        public void Init()
        {
            var mockEmployee = new Mock<IEmployee>();
            mockEmployee.SetupAllProperties();
            _dependent = new Dependent(mockEmployee.Object);
        }

        [Test]
        public void ShouldImplementDependentInterface()
        {
            Assert.IsInstanceOf<IDependent>(_dependent);
        }
    }
}